"use strict";

var fs = require('fs')
  , path = require('path')
  , morgan = require('morgan')
  , express = require('express')
  , next = require('next-port')
  , toobusy = require('toobusy')
  , app = express()
  , http = require('http').Server(app)
  , io = require('socket.io')(http)
  , saving = false

toobusy.maxLag(10)

app.use(function (req, res, next) {
  if (toobusy()) res.end(503, 'Server is busy.')
  else next()
})
app.use(morgan('dev'))
app.use(function (req, res, next) {
  if (req.path.substr(req.url.length - 1) === '/') {
    var url = req.path + 'index.html'
    res.sendFile(path.resolve(__dirname, '..', '.' + url))
  }
  else next()
}, express.static(path.resolve(__dirname, '..')))

io.on('connection', function (sock) {
  sock.on('update', function (post) {
    if (post.filename && !saving) {
      console.log('saving: "%s"', post.title)
      saving = true
      post.content = '# ' + post.title + '\n\n' + post.body + '\n'
      fs.writeFile(path.resolve(post.filename), post.content, function (err) {
        if (err) sock.emit('error', err)
        else sock.emit('ok', 'File saved.')
        saving = false
      })
    }
  })
})

next({
  lower: 8000
}, function (err, port) {
  if (err) process.exit(-1)
  http.listen(port, function () {
    console.log('LISTEN :%s', port)
  })
})
