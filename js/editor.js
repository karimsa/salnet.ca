(function () {
  "use strict";

  var app = angular.module('wallflower', [])
    , sock = io()
    , adjust = function () {
        var offset = $('textarea').offset()

        $('#markdown')
          .css('left', (offset.left - $('#markdown').width() - 10) + 'px')
          .css('top', offset.top + 'px')
      }

  adjust()
  $('.modal').modal()

  app.controller('editor', ['$scope', function ($scope) {
    window.$scope = $scope

    $('.ladda-button')
      .on('click', function (evt) {
        evt.preventDefault()
        $scope.$apply(function () {
          $scope.save()
        })
      })

    $scope.showSub = function () { if (!$scope.subtitle) { $('.subtitle').show(); adjust() } }
    $scope.hideSub = function () { if (!$scope.subtitle) { $('.subtitle').hide(); adjust() } }

    $scope.focus = function () {
      $('textarea').focus()
    }

    $scope.publish = function () {
      if ($scope.subtitle) {
        sock.once('ok', function () {
          $('.spinner').addClass('in')
          sock.emit('publish')
        })
        $scope.save()
      }
    }

    $scope.save = function () {
      $('.spinner:not(.in)').addClass('in')

      // do publish
      sock.emit('update', {
          title: $scope.title
        , filename: $scope.subtitle
        , body: $scope.body
      })
    }

    sock.on('ok', function () {
      setTimeout(function () {
        $('.spinner.in').removeClass('in')
      }, 1000)
    })

    $scope.$watch('title', $scope.save)
    //$scope.$watch('subtitle', $scope.save)
    $scope.$watch('body', $scope.save)
  }])
}())
