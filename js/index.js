// index.js
//
// Created by: Karim Alibhai
// Copyright (C) 2015 Salnet.

jQuery(document).ready(function ($) {
  "use strict";

  var width = window.innerWidth
    , contact = $('#contact')

  // decent sizing to allow avgrund
  // otherwise just redirect
  if (width >= 767) {
    $.ajax({
      url: '/contact.html',
      success: function (data) {
        console.log('configuring avgrund')
        contact.attr('href', '#').avgrund({
            width: 327
          , height: 180
          , showClose: true
          , showCloseText: 'Cancel'
          , closeByEscape: true
          , closeByDocument: true
          , onBlurContainer: '.avgrund-global'
          , enableStackAnimation: true
          , template: data
        })
      }
    })
  }
})
